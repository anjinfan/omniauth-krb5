require 'omniauth'

module OmniAuth
  module Strategies
    class Krb5
      include OmniAuth::Strategy

      args [:realm, :test_password]
      option :title, "Kerberos Authentication"

      def initialize( app, *args, &block )
        super
        unless options.test_password.present? && !options.test_password.empty?
          require 'rkerberos'
        end
      end
                 
      def request_phase
        OmniAuth::Form.build(:title => options.title, :url => callback_path) do
          text_field 'Username', 'username'
          password_field 'Password', 'password'
        end.to_response
      end

      def callback_phase
        begin
          return fail!(:invalid_credentials) unless username && password
          if options.test_password.present? && !options.test_password.empty?
            return fail!(:invalid_credentials) if password != options.test_password
          else
            @krb5 = Kerberos::Krb5.new
            @krb5.set_default_realm(options.realm)
            @krb5.get_init_creds_password(username_with_realm, password)
          end
          super          
        rescue Exception => e
          return fail!(:invalid_credentials, e)
        ensure
          @krb5.close unless @krb5.nil?
        end
      end
      
      uid do
        username_with_realm
      end
      
      info do 
        { :name  => username } 
      end

      def realm           
        options.realm
      end
      
      protected
        
        def username_with_realm
          user_with_realm = username.dup
          user_with_realm + "@#{realm}" unless username.include?('@')
        end
        
        def username
          request['username']
        end

        def password
          request['password']
        end

    end
  end
end
