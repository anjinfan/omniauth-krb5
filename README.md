OmniAuth::Krb5
==============

OmniAuth strategy for authenticating with Kerberos.

NOTE
====
This is a fork of original omniauth-krb5 gem available here: [https://github.com/naffis/omniauth-krb5](https://github.com/naffis/omniauth-krb5)

Installation and configuration
==============================

Install manually or using Bundler:

```bash
gem 'omniauth-krb5', :git => 'https://bibucket.org/anjinfan/omniauth-krb5.git'
```

Add Kerberos provider to omniauth builder:

```ruby
use OmniAuth::Builder do
  provider :krb5, "CUSTOM.REALM.COM"
  # provider ...
end
```

If you want to test your application without the need to connect every developer to Kerberos:

```ruby
use OmniAuth::Builder do
  provider :krb5, "CUSTOM.REALM.COM", "DevelopmentPassword123"
  # provider ...
end
```

The second parameter triggers test mode, where no Kerberos connection is made and instead every user password is compared
to the value of this parameter ("DevelopmentPassword123" in the example).
